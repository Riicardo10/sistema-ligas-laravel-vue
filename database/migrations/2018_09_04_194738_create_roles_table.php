<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesTable extends Migration {
    
    public function up() {
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 30)->unique();
            $table->string('descripcion', 100)->nullable();
            $table->boolean('estado')->default(1);
            $table->timestamps();
        });
        DB::table('roles')->insert(array('id'=>1, 'nombre'=>'admin', 'descripcion'=>'Administrador del sitio'));
        DB::table('roles')->insert(array('id'=>2, 'nombre'=>'organizador', 'descripcion'=>'Organizador de una liga'));
        DB::table('roles')->insert(array('id'=>3, 'nombre'=>'invitado', 'descripcion'=>'Invitado'));
    }

    public function down() {
        Schema::dropIfExists('roles');
    }
}
