<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRolUsersTable extends Migration {
    public function up() {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('estado')->default(1);
            $table->integer('id_rol')->unsigned();
            $table->foreign('id_rol')->references('id')->on('roles');
        } );
    }

    public function down() {
        
    }
}
