<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJugadoresTable extends Migration {
    
    public function up() {
        Schema::create('jugadores', function (Blueprint $table) {
            $table->string('curp', 18);
            $table->primary('curp');
            $table->string('nombre', 150);
            $table->string('foto', 360)->nullable();
            $table->date('fecha_nacimiento')->nullable();
            $table->string('sexo', 1)->nullable();
            $table->timestamps();
        });
    }

    public function down() {
        Schema::dropIfExists('jugadores');
    }
}
