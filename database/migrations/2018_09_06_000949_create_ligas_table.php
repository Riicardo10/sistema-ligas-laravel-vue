<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLigasTable extends Migration {
    
    public function up() {
        Schema::create('ligas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->integer('id_ciudad')->unsigned();
            $table->foreign('id_ciudad')->references('id')->on('ciudades');
            $table->integer('anio');
            $table->integer('id_usuario')->unsigned();
            $table->foreign('id_usuario')->references('id')->on('users');
            $table->timestamps();
        });
    }

    public function down() {
        Schema::dropIfExists('ligas');
    }
}
