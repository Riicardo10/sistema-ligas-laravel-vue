<?php


Route::get('/', function () {
    return view( 'principal' );
});

Route::resource( '/estado', 'EstadoController' );
Route::get( '/estados', 'EstadoController@getEstados' );
Route::resource( '/ciudad', 'CiudadController' );
Route::resource( '/categoria', 'CategoriaController' );
Route::resource( '/jugador', 'JugadorController' );
Route::resource( '/rol', 'RolController' );
Route::get( '/roles', 'RolController@getRoles' );
Route::get( '/ciudades', 'CiudadController@getCiudades' );
Route::put( '/rol/activar/id', 'RolController@activar' );
Route::put( '/rol/desactivar/id', 'RolController@desactivar' );

Route::resource( '/usuario', 'UserController' );
Route::get( '/usuarios', 'UserController@getUsuarios' );
Route::put( '/usuario/activar/id', 'UserController@activar' );
Route::put( '/usuario/desactivar/id', 'UserController@desactivar' );
Route::resource( '/liga', 'LigaController' );

Route::get( '/ligas/listar/pdf', 'LigaController@getLigasPDF' );