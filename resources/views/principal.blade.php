<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Sistema Ligas Laravel Vue Js - Ricardo Flores">
    <meta name="author" content="Ricardo Flores">
    <meta name="keyword" content="Sistema Ligas Laravel Vue Js">
    <link rel="shortcut icon" href="/img/favicon.png">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Ligas Baloncesto</title>
    <link href="/css/plantilla.css" rel="stylesheet">
</head>

<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">

    <div id="app">

        <header class="app-header navbar">
            <button class="navbar-toggler mobile-sidebar-toggler d-lg-none mr-auto" type="button">
            <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand" href="#"></a>
            <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button">
            <span class="navbar-toggler-icon"></span>
            </button>
            <ul class="nav navbar-nav d-md-down-none">
                <li @click="menu=0" class="nav-item px-3">
                    <a class="nav-link" href="#">Escritorio</a>
                </li>
                <li class="nav-item px-3">
                    <a class="nav-link" href="#">Configuraciones</a>
                </li>
            </ul>
            <ul class="nav navbar-nav ml-auto">
                <li class="nav-item d-md-down-none">
                    <a class="nav-link" href="#" data-toggle="dropdown">
                        <i class="icon-bell"></i>
                        <span class="badge badge-pill badge-danger">5</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <div class="dropdown-header text-center">
                            <strong>Notificaciones</strong>
                        </div>
                        <a class="dropdown-item" href="#">
                            <i class="fa fa-envelope-o"></i> Ingresos
                            <span class="badge badge-success">3</span>
                        </a>
                        <a class="dropdown-item" href="#">
                            <i class="fa fa-tasks"></i> Ventas
                            <span class="badge badge-danger">2</span>
                        </a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        <img src="/img/avatars/6.jpg" class="img-avatar" alt="admin@bootstrapmaster.com">
                        <span class="d-md-down-none">admin </span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <div class="dropdown-header text-center">
                            <strong>Cuenta</strong>
                        </div>
                        <a class="dropdown-item" href="#"><i class="fa fa-user"></i> Perfil</a>
                        <a class="dropdown-item" href="#"><i class="fa fa-lock"></i> Cerrar sesión</a>
                    </div>
                </li>
            </ul>
        </header>

        <div class="app-body">
            <div class="sidebar">
                <nav class="sidebar-nav">
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link active" href="main.html"><i class="icon-"></i> Escritorio</a>
                        </li>
                        <li class="nav-title">
                            Mantenimiento
                        </li>
                        <li class="nav-item nav-dropdown">
                            <a class="nav-link nav-dropdown-toggle" href="#"><i class="icon-"></i> Lugares</a>
                            <ul class="nav-dropdown-items" >
                                <li @click="menu=1" class="nav-item" style="background: #4A464B;">
                                    <a class="nav-link" href="#" style="color:#FFF;"><i class="icon-"></i> Estados</a>
                                </li>
                                <li @click="menu=2" class="nav-item" style="background: #4A464B;">
                                    <a class="nav-link" href="#" style="color:#FFF;"><i class="icon-"></i> Ciudades</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item nav-dropdown">
                            <a class="nav-link nav-dropdown-toggle" href="#"><i class="icon-"></i> Ligas</a>
                            <ul class="nav-dropdown-items">
                                <li @click="menu=3" class="nav-item" style="background: #4A464B;">
                                    <a class="nav-link" href="i#" style="color:#FFF;"><i class="icon-"></i> Liga</a>
                                </li>
                                <li @click="menu=4" class="nav-item" style="background: #4A464B;">
                                    <a class="nav-link" href="#" style="color:#FFF;"><i class="icon-"></i> Categoria</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item nav-dropdown">
                            <a class="nav-link nav-dropdown-toggle" href="#"><i class="icon-"></i> Equipos</a>
                            <ul class="nav-dropdown-items">
                                <li @click="menu=5" class="nav-item" style="background: #4A464B;">
                                    <a class="nav-link" href="i#" style="color:#FFF;"><i class="icon-"></i> Equipo</a>
                                </li> 
                                <li @click="menu=6" class="nav-item" style="background: #4A464B;">
                                    <a class="nav-link" href="#" style="color:#FFF;"><i class="icon-"></i> Jugador</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item nav-dropdown">
                            <a class="nav-link nav-dropdown-toggle" href="#"><i class="icon-"></i> Acceso</a>
                            <ul class="nav-dropdown-items">
                                <li @click="menu=7" class="nav-item" style="background: #4A464B;">
                                    <a class="nav-link" href="i#" style="color:#FFF;"><i class="icon-"></i> Usuarios</a>
                                </li>
                                <li @click="menu=8" class="nav-item" style="background: #4A464B;">
                                    <a class="nav-link" href="#" style="color:#FFF;"><i class="icon-"></i> Roles</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item nav-dropdown">
                            <a class="nav-link nav-dropdown-toggle" href="#"><i class="icon-"></i> Reportes</a>
                            <ul class="nav-dropdown-items">
                                <li @click="menu=9" class="nav-item" style="background: #4A464B;">
                                    <a class="nav-link" href="i#" style="color:#FFF;"><i class="icon-"></i> Lista de ligas</a>
                                </li>                                
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="main.html"><i class="icon-"></i> Ayuda <span class="badge badge-danger">PDF</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="main.html"><i class="icon-"></i> Acerca de...<span class="badge badge-info">IT</span></a>
                        </li>
                    </ul>
                </nav>
                <button class="sidebar-minimizer brand-minimizer" type="button"></button>
            </div>


            <template v-if="menu==0">
                <dashboard></dashboard>
            </template>
            <template v-if="menu==1">
                <estado></estado>
            </template>
            <template v-if="menu==2">
                <ciudad></ciudad>
            </template>
            <template v-if="menu==3">
                <liga></liga>
            </template>
            <template v-if="menu==4">
                <categoria></categoria>
            </template>
            <template v-if="menu==5">
                <!--<equipo></equipo>-->
                <h1>Contenido del equipo</h1>
            </template>
            <template v-if="menu==6">
                <jugador></jugador>
            </template>
            <template v-if="menu==7">
                <usuario></usuario>
            </template>
            <template v-if="menu==8">
                <rol></rol>
            </template>
            <template v-if="menu==9">
                <consulta_ligas></consulta_ligas>
            </template>
            

            <!-- Contenido Principal -->
            
            <!--<ciudad></ciudad>-->
            <!--<categoria></categoria>-->
            <!--<jugador></jugador>-->
            <!-- /Fin del contenido principal -->
            
        </div>
        <pie></pie>
    </div>

    <script src="/js/app.js"></script>
    <script src="/js/plantilla.js"></script>
    
</body>
</html>