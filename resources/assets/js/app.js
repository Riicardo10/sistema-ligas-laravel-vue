require('./bootstrap');

window.Vue = require('vue');

Vue.component('dashboard', require('./components/Dashboard.vue'));
Vue.component('pie', require('./components/PiePagina.vue'));
Vue.component('estado', require('./components/Estado.vue'));
Vue.component('ciudad', require('./components/Ciudad.vue'));
Vue.component('categoria', require('./components/Categoria.vue'));
Vue.component('jugador', require('./components/Jugador.vue'));
Vue.component('rol', require('./components/Rol.vue'));
Vue.component('usuario', require('./components/Usuario.vue'));
Vue.component('liga', require('./components/Liga.vue'));
Vue.component('consulta_ligas', require('./components/ConsultaLigas.vue'));

const app = new Vue({
    el: '#app',
    data: {
        menu: 0,
    },
});
