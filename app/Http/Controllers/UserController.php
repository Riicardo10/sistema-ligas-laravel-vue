<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;

class UserController extends Controller {
    
    public function index( Request $request ) {
        if( $request->buscar == '' ) 
            $usuarios = User::join('roles', 'users.id_rol', '=', 'roles.id')
            ->select('users.id', 'users.name as usuario','users.email','users.estado','roles.nombre as rol', 'roles.id as id_rol')
            ->orderBy('users.id', 'desc')->paginate(3);
        else
            $usuarios = User::join('roles', 'users.id_rol', '=', 'roles.id')
            ->select('users.id', 'users.name as usuario','users.email','users.estado','roles.nombre as rol', 'roles.id as id_rol')
            ->where('users.email', 'like', '%' . $request->buscar . '%')
            ->orderBy('users.id', 'desc')->paginate(3);
        return [
            'paginacion' => [
                'total' => $usuarios->total(),
                'pagina_actual' => $usuarios->currentPage(),
                'por_pagina' => $usuarios->perPage(),
                'ultima_pagina' => $usuarios->lastPage(),
                'desde' => $usuarios->firstItem(),
                'hasta' => $usuarios->lastItem()
            ],
            'usuarios' => $usuarios
        ];
    }

    public function store(Request $request) {
        $usuario = new User();
        $usuario->name = $request->usuario;
        $usuario->password = $request->password;
        $usuario->email = $request->email;
        $usuario->estado = '1';
        $usuario->id_rol = $request->id_rol;
        $usuario->save();
    }

    public function update(Request $request, $id) {
        $usuario = User::findOrFail( $request->id );
        $usuario->name = $request->usuario;
        $usuario->email = $request->email;
        $usuario->id_rol = $request->id_rol;
        
        $usuario->save();   
    }

    public function activar(Request $request) {
        $usuario = User::findOrFail( $request->id );
        $usuario->estado = '1';
        $usuario->save();   
    }
    public function desactivar(Request $request) {
        $usuario = User::findOrFail( $request->id );
        $usuario->estado = '0';
        $usuario->save();   
    }

    public function getUsuarios(Request $request) {
        $usuarios = User::select('id', 'name', 'email')->orderBy('name', 'asc')->get();
        return [
            'usuarios' => $usuarios
        ];
    }

}
