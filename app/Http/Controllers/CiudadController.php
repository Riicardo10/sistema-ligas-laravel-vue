<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Ciudad;

class CiudadController extends Controller {
    
    public function index(Request $req) {
        if( $req->buscar == '' ) 
            $ciudades = Ciudad::join('estados', 'estados.id', '=', 'ciudades.id_estado')
                ->select('estados.id as id_estado', 'estados.nombre as estado',
                         'ciudades.id as id_ciudad', 'ciudades.nombre as ciudad')
                ->orderBy('ciudades.id', 'desc')->paginate(3);
        else
            $ciudades = Ciudad::join('estados', 'estados.id', '=', 'ciudades.id_estado')
                ->select('estados.id as id_estado', 'estados.nombre as estado',
                         'ciudades.id as id_ciudad', 'ciudades.nombre as ciudad')
                ->where('ciudades.nombre', 'like', '%' . $req->buscar . '%')
                ->orderBy('ciudades.id', 'desc')->paginate(3);
        return [
            'paginacion' => [
                'total' => $ciudades->total(),
                'pagina_actual' => $ciudades->currentPage(),
                'por_pagina' => $ciudades->perPage(),
                'ultima_pagina' => $ciudades->lastPage(),
                'desde' => $ciudades->firstItem(),
                'hasta' => $ciudades->lastItem()
            ],
            'ciudades' => $ciudades
        ];
    }

    public function store(Request $request) {
        $ciudad = new Ciudad();
        $ciudad->nombre = $request->nombre;
        $ciudad->id_estado = $request->id_estado;
        $ciudad->save();
    }

    public function update(Request $request, $id) {
        $ciudad = Ciudad::findOrFail( $request->id );
        $ciudad->nombre = $request->nombre;
        $ciudad->id_estado = $request->id_estado;
        $ciudad->save();   
    }

    public function getCiudades(Request $request) {
        $ciudades = Ciudad::select('id', 'nombre')->orderBy('nombre', 'asc')->get();
        return [
            'ciudades' => $ciudades
        ];
    }
    
}
