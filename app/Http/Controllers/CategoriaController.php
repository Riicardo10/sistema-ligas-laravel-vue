<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Categoria;

class CategoriaController extends Controller {
    public function index( Request $request ) {
        if( $request->buscar == '' ) 
            $categorias = Categoria::orderBy('categorias.id', 'desc')->paginate(3);
        else
            $categorias = Categoria::where('categorias.nombre', 'like', '%' . $request->buscar . '%')->orderBy('categorias.id', 'desc')->paginate(3);
        return [
            'paginacion' => [
                'total' => $categorias->total(),
                'pagina_actual' => $categorias->currentPage(),
                'por_pagina' => $categorias->perPage(),
                'ultima_pagina' => $categorias->lastPage(),
                'desde' => $categorias->firstItem(),
                'hasta' => $categorias->lastItem()
            ],
            'categorias' => $categorias
        ];
    }

    public function store(Request $request) {
        $categoria = new Categoria();
        $categoria->nombre = $request->nombre;
        $categoria->descripcion = $request->descripcion;
        $categoria->save();
    }

    public function update(Request $request, $id) {
        $categoria = Categoria::findOrFail( $request->id );
        $categoria->nombre = $request->nombre;
        $categoria->descripcion = $request->descripcion;
        $categoria->save();   
    }

    public function getCategorias() {
        $categorias = Categoria::all();
        return [ 'categorias' => $categorias ];
    }
}
