<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Liga;

class LigaController extends Controller {

    public function index( Request $request ) {
        if( $request->buscar == '' ) 
            $ligas = Liga::join('ciudades', 'ligas.id_ciudad', '=', 'ciudades.id')
            ->join('users', 'ligas.id_usuario', '=', 'users.id')
            ->select('ligas.id', 'ligas.nombre as liga','ligas.anio','ciudades.id as id_ciudad','ciudades.nombre as ciudad', 'users.id as id_usuario', 'users.name as usuario', 'users.email')
            ->orderBy('ligas.id', 'desc')->paginate(3);
        else
            $ligas = Liga::join('ciudades', 'ligas.id_ciudad', '=', 'ciudades.id')
            ->join('users', 'ligas.id_usuario', '=', 'users.id')
            ->select('ligas.id', 'ligas.nombre as liga','ligas.anio','ciudades.id as id_ciudad','ciudades.nombre as ciudad', 'users.id as id_usuario', 'users.name as usuario', 'users.email')
            ->where('ligas.nombre', 'like', '%' . $request->buscar . '%')
            ->orderBy('ligas.id', 'desc')->paginate(3);
        return [
            'paginacion' => [
                'total' => $ligas->total(),
                'pagina_actual' => $ligas->currentPage(),
                'por_pagina' => $ligas->perPage(),
                'ultima_pagina' => $ligas->lastPage(),
                'desde' => $ligas->firstItem(),
                'hasta' => $ligas->lastItem()
            ],
            'ligas' => $ligas
        ];
    }

    public function store(Request $request) {
        $liga = new Liga();
        $liga->nombre = $request->nombre;
        $liga->anio = $request->anio;
        $liga->id_usuario = $request->id_usuario;
        $liga->id_ciudad = $request->id_ciudad;
        $liga->save();
    }
    public function update(Request $request, $id) {
        $liga = Liga::findOrFail( $request->id );
        $liga->nombre = $request->nombre;
        $liga->id_ciudad = $request->id_ciudad;
        $liga->id_usuario = $request->id_usuario;
        $liga->anio = $request->anio;
        $liga->save();   
    }

    public function getLigasPDF(){
        $ligas = Liga::join('ciudades', 'ligas.id_ciudad', '=', 'ciudades.id')
            ->join('users', 'ligas.id_usuario', '=', 'users.id')
            ->select('ligas.id', 'ligas.nombre as liga','ligas.anio','ciudades.id as id_ciudad','ciudades.nombre as ciudad', 'users.id as id_usuario', 'users.name as usuario', 'users.email')
            ->orderBy('ligas.id', 'desc')->paginate(3);
        $cantidad = Liga::count();
        $pdf = \PDF::loadView( 'pdf.ligas_pdf', ['ligas'=>$ligas, 'cantidad'=>$cantidad] );
        return $pdf->download( 'ligas.pdf' );
    }

}
