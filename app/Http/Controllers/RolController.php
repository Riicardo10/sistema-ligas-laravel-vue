<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Rol;

class RolController extends Controller {
    public function index( Request $request ) {
        if( $request->buscar == '' ) 
            $roles = Rol::orderBy('id', 'desc')->paginate(3);
        else
            $roles = Rol::where('nombre', 'like', '%' . $request->buscar . '%')->orderBy('id', 'desc')->paginate(3);
        return [
            'paginacion' => [
                'total' => $roles->total(),
                'pagina_actual' => $roles->currentPage(),
                'por_pagina' => $roles->perPage(),
                'ultima_pagina' => $roles->lastPage(),
                'desde' => $roles->firstItem(),
                'hasta' => $roles->lastItem()
            ],
            'roles' => $roles
        ];
    }

    public function store(Request $request) {
        $rol = new Rol();
        $rol->nombre = $request->nombre;
        $rol->descripcion = $request->descripcion;
        $rol->estado = '1';
        $rol->save();
    }

    public function update(Request $request, $id) {
        $rol = Rol::findOrFail( $request->id );
        $rol->nombre = $request->nombre;
        $rol->descripcion = $request->descripcion;
        $rol->save();   
    }

    public function getRoles(Request $request) {
        $roles = Rol::where('estado', '=', '1')
            ->select('id', 'nombre')->orderBy('nombre', 'asc')->get();
        return [
            'roles' => $roles
        ];
    }


    public function activar(Request $request) {
        $rol = Rol::findOrFail( $request->id );
        $rol->estado = '1';
        $rol->save();   
    }
    public function desactivar(Request $request) {
        $rol = Rol::findOrFail( $request->id );
        $rol->estado = '0';
        $rol->save();   
    }
}
