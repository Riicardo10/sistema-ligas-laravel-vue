<?php
// https://appdividend.com/2018/02/13/vue-js-laravel-file-upload-tutorial/
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Jugador;

class JugadorController extends Controller {
    
    public function index( Request $request ) {
        if( $request->buscar == '' ) 
            $jugadores = Jugador::orderBy('updated_at', 'desc')->paginate(3);
        else
            $jugadores = Jugador::where('nombre', 'like', '%' . $request->buscar . '%')->orderBy('updated_at', 'desc')->paginate(3);
        return [
            'paginacion' => [
                'total' => $jugadores->total(),
                'pagina_actual' => $jugadores->currentPage(),
                'por_pagina' => $jugadores->perPage(),
                'ultima_pagina' => $jugadores->lastPage(),
                'desde' => $jugadores->firstItem(),
                'hasta' => $jugadores->lastItem()
            ],
            'jugadores' => $jugadores
        ];
    }

    public function store(Request $request) {
        if( $request->get('image' )){
            $image = $request->get('image');
            $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
            \Image::make($request->get('image'))->save(public_path('images/').$name);
        }
        else{
            $name = 'default.png';
        }
        $jugador = new Jugador();
        $jugador->curp = strtoupper($request->curp);
        $jugador->nombre = ucwords(strtolower($request->nombre));
        $jugador->fecha_nacimiento = $request->fecha_nacimiento;
        $jugador->sexo = $request->sexo;
        $jugador->foto = $name;
        $jugador->save();
    }

    public function update(Request $request, $curp) {;
        if( $request->get('image' )){
            $image = $request->get('image');
            $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
            \Image::make($request->get('image'))->save(public_path('images/').$name);
            Jugador::where( 'curp', $curp )->update( [
                'nombre'=>ucwords(strtolower($request->nombre)),
                'sexo'=>$request->sexo,
                'fecha_nacimiento'=>$request->fecha_nacimiento,
                'foto'=>$name
            ] );
        }
        else{
            Jugador::where( 'curp', $curp )->update( [
                'nombre'=>ucwords(strtolower($request->nombre)),
                'sexo'=>$request->sexo,
                'fecha_nacimiento'=>$request->fecha_nacimiento
            ] );
        }
    }

    public function getEstados() {
        $estados = Estado::all();
        return [ 'estados' => $estados ];
    }

}
