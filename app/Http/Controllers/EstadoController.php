<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Estado;

class EstadoController extends Controller {
    
    public function index( Request $request ) {
        if( $request->buscar == '' ) 
            $estados = Estado::orderBy('estados.id', 'desc')->paginate(3);
        else
            $estados = Estado::where('estados.nombre', 'like', '%' . $request->buscar . '%')->orderBy('estados.id', 'desc')->paginate(3);
        return [
            'paginacion' => [
                'total' => $estados->total(),
                'pagina_actual' => $estados->currentPage(),
                'por_pagina' => $estados->perPage(),
                'ultima_pagina' => $estados->lastPage(),
                'desde' => $estados->firstItem(),
                'hasta' => $estados->lastItem()
            ],
            'estados' => $estados
        ];
    }

    public function store(Request $request) {
        $estado = new Estado();
        $estado->nombre = $request->nombre;
        $estado->save();
    }

    public function update(Request $request, $id) {
        $estado = Estado::findOrFail( $request->id );
        $estado->nombre = $request->nombre;
        $estado->save();   
    }

    public function getEstados() {
        $estados = Estado::all();
        return [ 'estados' => $estados ];
    }

}
