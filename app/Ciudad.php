<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Ciudad extends Model {
    
    protected $table = 'ciudades';
    public $timestamps = false;

    public function estado() {
        //return $this->hasOne( 'App\Estado' );
        //return $this->belongsTo( 'App\Estado' );
    }

}
